typedef struct skip_elt {
    int value;
    int level;
    struct skip_elt **next;
} skip_elt;

typedef struct skip_list {
    int nbLevels;
    struct skip_elt **first;
} skip_list;


/**
 * Fonction d'initialisation de la skiplist
 * @return Un pointeur skip_list initialisé en mémoire, ou NULL si l'allocation échoue
 */
skip_list *skiplist_init(void);

/**
 * Fonction de destruction de la skiplist
 * @param list le pointeur de la liste à détruire
 */
void skiplist_destruct(skip_list * list);

/**
 * Constructeur de skip_elt. Fabrique un skip_elt de niveau level avec la valuer value, ne pointant sur rien
 * @param value la valeur du bloc
 * @param level le niveau max du bloc a creer
 * @return un pointeur vers la zone memoire allouée
 */
skip_elt **skipelt_create(int value, int level);

/**
 * @param list : La liste
 * Test l'existence d'une valeur au sein d'un skip_list
 * @param elt : l'element dont on teste la presence
 * @return 1 si la valeur elt est présente dans la liste, 0 sinon
 */
int skiplist_exist(skip_list *list, int elt);


/**
 * Recherche un element au sein d'une skip_list et retourne le pointeur vers l'element
 * @param list : la liste
 * @param elt : la valeur de l'element à rechercher
 * @return Le pointeur vers le tableau de l'element si il existe, NULL sinon
 */
skip_elt **skiplist_search(skip_list *list, int elt);


/**
 * Recherche l'element precedent de la chaine à un niveau donné
 * @param list : la liste
 * @param elt : l'element
 * @param level : une hauteur
 * @return Un pointeur vers l'element précédant elt au niveau level si il existe, NULL sinon
 */
skip_elt *skiplist_getLast(skip_list *list, int elt, int level);

/**
 * Recupere la table des predecesseurs d'un element a tout les niveaux d'un element
 * @param list : la liste
 * @param elt : l'element
 * @param result : la table resultante
 * @return le nombre de predeceusseur
 */
skip_elt **skiplist_previous(skip_list *list, int elt, skip_elt **result);


/**
 * Insert un element dans une skip_list
 * @param list : la liste
 * @param elt : un element (int) à ajouter
 * @param p : la probabilité du coin flip pour le niveau
 * @return 1 si la valeur est dans la liste, 0 sinon
 */
int skiplist_insert(skip_list *list, int elt, double p);

/* Fonctions auxiliaires d'insertion */
int skiplist_insert_first(skip_list *list, int elt, double p);
int skiplist_insert_next(skip_list *list, int last, int elt, double p); // utile ?
int skiplist_insert_last(skip_list *list, int elt, double p);


/**
 * Supression d'un element dans une skip_list
 * @param list : la liste
 * @param elt : Un element à supprimer
 * @return 1 si la valeur a bien été supprimée, 0 si elle n'était pas présente
 */
int skiplist_delete(skip_list *list, int elt);

/* Fonctions auxiliaires de suppression */
int skiplist_delete_first(skip_list *list);
int skiplist_delete_next(skip_list *list, int elt);
int skiplist_delete_last(skip_list *list); // inutile

/* Déterminer aléatoiremnt le niveau d'un element de la skiplist */
int choose_level(double p);

/**
 * Affiche la skip_list list sur la sortie standard
 * @param list : la liste
 */
void skiplist_print(skip_list *list);
