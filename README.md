# README

## Intro
Ce projet a été réalisé par ALVES Alexandre et LECLERC Victor.

Il permet la mise en place de la structure des skip list.

## Compilation & Exécution
### Dépendances
CMake est nécessaire à la compilation de ce projet

### Commande
Déplacement dans le répertoire adapté:
	$ cd build

Génération du Makefile:
	$ cmake ..

Compilation de l'exécutable:
	$ make

Lancement du programme exemple:
	$ ./main

## Preuve
### Commande
En ligne de commande:
	$ frama-c -wp -wp-split -wp-split-depth -1 -wp-init-const -val-ignore-recursive-calls main.c skiplist.{c,h}

Utilisant la version intéractive de frama-c:
	$ frama-c-gui -wp -wp-split -wp-split-depth -1 -wp-init-const -val-ignore-recursive-calls main.c skiplist.{c,h}

### Paramètres
* -wp: Utilisation de wp.
* -wp-rte: Génération des gardes RTE avant wp.
* -wp-init-const: Utilisation des "initializers" pour les constantes globales.
* -wp-split: Séparation des conjonctions.
* -wp-split-depth <p>: Séparation des conjonctions <p> fois (-1 = illimité).
* -val-ignore-recursive-calls: Ignorer les appels récursifs.

## Sapins
Joyeux Noël :D

         X
        /|\
       /@|\\
      ///|\\\
      ///|\@\
     //@/|\\\\
    /////|\\@\\
        |||

         X
        /|\
       /@|\\
       //|\\
      ///|\@\
      /@/|\\\
     ////|\\@\
        |||


         X
        /|\
      //@|\\\
       //|\\
     ////|\@\\
      /@/|\\\
    /////|\\@\\
        |||


         _   ____
        |_|  |___|


          X
         /|\
        /@|\\
        //|\\
      ////|\@\\
      //@/|\\\\
    //////|\\@\\\
         |||
