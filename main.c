#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include "skiplist.h"

/* UNUSED (yet) */
#include <math.h>
#include <string.h>
#include <errno.h>


int main(int argc __attribute__((unused)),char **argv __attribute__((unused)))
{
	/* Test de Rand */
	srand(time(NULL));
	// printf("%f\n", (double) rand()/RAND_MAX);
	// printf("%d\n", RAND_MAX);
	// printf("%f\n", (double) 1/2);

	// printf("nb level = %d \n", choose_level(0.5));
	// printf("nb level = %d \n", choose_level(0.5));
	// printf("nb level = %d \n", choose_level(0.5));
	// printf("nb level = %d \n", choose_level(0.5));
	// printf("nb level = %d \n", choose_level(0.5));

	/* Définition des listes */
	skip_list *list1;
	skip_list *list2;
	skip_list *list3;

	/* Initialisation des listes */
	list1 = skiplist_init();
	list2 = skiplist_init();
	list3 = skiplist_init();

	if( list1 ){
		/* Insertion de valeurs dans les listes */
		skiplist_insert(list1,3,0.5);
		skiplist_insert(list1,5,0.5);
		skiplist_insert(list1,7,0.5);
		/* Insert en doublon */
		skiplist_insert(list1,3,0.5);
		skiplist_insert(list1,5,0.5);
		skiplist_insert(list1,7,0.5);
		/* insertions de nouvelles valeurs intermediaires */
		skiplist_insert(list1,4,0.5);
		skiplist_insert(list1,6,0.5);
		/* Insertion en tête de liste */
		skiplist_insert(list1,2,0.5);
		skiplist_insert(list1,1,0.5);
		/* Insertion en queue */
		skiplist_insert(list1,9,0.5);
		skiplist_insert(list1,10,0.5);
		/* Insertion intermediaire hauteur max */
		skiplist_insert(list1,8,0.5);
		/* Affichage des listes */
		printf("liste 1 : \n");
		skiplist_print (list1);

		/* Tests getlast */
		printf("getLast 1... \n");
		skip_elt* test = skiplist_getLast(list1, 1, 0);
		printf("getLast 2... \n");
		skip_elt* test2 = skiplist_getLast(list1, 1, 1);
		printf("getLast 3... \n");
		skip_elt* test3 = skiplist_getLast(list1, 3, 1);
		printf("getLast 4... \n");
		skip_elt* test4 = skiplist_getLast(list1, 4, 1);
		printf("getLast 5... \n");
		skip_elt* test5 = skiplist_getLast(list1, 5, 1);
		printf("getLast 6... \n");
		skip_elt* test6 = skiplist_getLast(list1, 6, 1);

		/* Suppresion */
		printf("suppression de 1 dans liste 1... \n");
		skiplist_delete(list1, 1);
		printf("liste 1 apres suppression de 1 : \n");
		skiplist_print (list1);

		printf("suppression de 2 dans liste 1... \n");
		skiplist_delete(list1, 2);
		skiplist_print (list1);
		printf("suppression de 4 dans liste 1... \n");
		skiplist_delete(list1, 4);
		skiplist_print (list1);
		printf("suppression de 6 dans liste 1... \n");
		skiplist_delete(list1, 6);
		skiplist_print (list1);
		printf("suppression de 8 dans liste 1... \n");
		skiplist_delete(list1, 8);
		skiplist_print (list1);
		printf("suppression de 10 dans liste 1... \n");
		skiplist_delete(list1, 10);
		printf("liste 1 apres suppression des pairs : \n");
		skiplist_print (list1);

		/* Reinsertion des impairs */
		skiplist_insert(list1,2,0.5);
		skiplist_insert(list1,4,0.5);
		skiplist_insert(list1,6,0.5);
		skiplist_insert(list1,8,0.5);
		skiplist_insert(list1,10,0.5);
		printf("liste 1 apres reinsertion des pairs : \n");
		skiplist_print (list1);

		/* Suppresioin des pairs */
		skiplist_delete(list1, 1);
		skiplist_delete(list1, 3);
		skiplist_delete(list1, 5);
		skiplist_delete(list1, 7);
		skiplist_delete(list1, 9);
		printf("liste 1 apres suppression des impairs : \n");
		skiplist_print(list1);

		printf("\n\n");
	}

	if( list2 ){
		skiplist_insert(list2,10,1);
		skiplist_insert(list2,20,1);
		skiplist_insert(list2,23,0.5);
		skiplist_insert(list2,24,1);
		skiplist_insert(list2,22,0.5);
		skiplist_insert(list2,15,0.5);
		skiplist_insert(list2,14,0.5);
		skiplist_insert(list2,27,0.5);
		skiplist_insert(list2,11,0.5);
		skiplist_insert(list2,7,0.5);
		//printf("liste 2 : \n");
		//skiplist_print (list2);
	}

	if( list3 ){
		skiplist_insert(list3, 3, 0.5);
		skiplist_insert(list3, 6, 0.5);
		skiplist_insert(list3, 10, 0.5);
		skiplist_insert(list3, 2, 0.5);
		skiplist_insert(list3, 8, 0.5);

		skiplist_print(list3);
	}

	/* Sortie du programme*/
	return EXIT_SUCCESS;
}
