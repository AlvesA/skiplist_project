#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <time.h>

#include <errno.h>
#include <assert.h>

#include "skiplist.h"

#define SKIPLIST_MAX_LEVEL 5

/****************************************************
	Atteindre un élément par induction.
 ****************************************************/
/*@ inductive reachable{L} (skip_elt** first, skip_elt** node) {
		case root_reachable{L}:
			\forall skip_elt** first; reachable(first,first);
		case next_reachable{L}:
			\forall	skip_elt** first, **node;
			\valid(first) ==> reachable(first[0]->next, node) ==> reachable(first,node);
	}
*/
/****************************************************
	Predicat: la liste est finie.
 ****************************************************/
/*@ predicate finite{L}(skip_elt** first) = reachable(first, \null); */

/****************************************************
	Permet de calculer la taille d'une liste.
 ****************************************************/
/*@	axiomatic Length {
		logic integer length{L}(skip_elt** e);

		axiom length_nil{L}: length(\null) == 0;

		axiom length_cons{L}:
			\forall skip_elt** e, integer n;
				finite(e) && \valid(e) ==>
					length(e) == length(e[0]->next) + 1;
	}

 */

/****************************************************
	Garantie que le type skiplist est fini.
 ****************************************************/
/*@ type invariant finite_list(skip_list* list) = finite(list->first); */
/****************************************************
	Une liste est triee par ordre croissant.
 ****************************************************/
/*@ inductive sorted_increasing{L}(skip_elt** first) {
		case sorted_nil{L}: sorted_increasing(\null);

		case sorted_singleton{L}:
			\forall skip_elt** first;
				\valid(first) && first[0]->next == \null ==>
					sorted_increasing(first);

		case sorted_next{L}:
			\forall skip_elt** first;
			\valid(first) && \valid(first[0]->next) &&
			first[0]->value < first[0]->next[0]->value &&
			sorted_increasing(first[0]->next) ==> sorted_increasing(first);
	}
 */
/****************************************************
	Le type skip_list est trie par ordre croissant.
 ****************************************************/
/*@	type invariant skiplist_sorted(skip_list* list) = sorted_increasing(list->first); */
/****************************************************
	1: l'element est dans la liste.
	0: sinon.
 ****************************************************/
/*@ axiomatic Count {
		logic integer count(int x,skip_elt** l);

		axiom count_nil: \forall int x; count(x,\null) == 0;

		axiom count_cons_head{L}:
			\forall int x,skip_elt** l;
				\valid(l) && l[0]->value == x ==>
					count(x,l) == count(x,l[0]->next)+1;

		axiom count_cons_tail{L}:
			\forall int x, skip_elt** l;
				\valid(l) && l[0]->value != x ==>
					count(x,l) == count(x,l[0]->next);
	}
*/


/****************************************************
	2 listes sont identiques par induction.
 ****************************************************/
/*@ inductive same{L} (skip_elt** first1, skip_elt** first2) {
		case root_same{L}:
			\forall skip_elt **first1, **first2; same(first1, first2);
		case next_same{L}:
			\forall	skip_elt** first1, **first2;
			\valid(first1) ==> \valid(first2) ==> same(first1[0]->next, first2[0]->next) ==> reachable(first1, first2);
	}
*/


skip_list *skiplist_init(void) {
	skip_list *list;
	/* Création et initialisation de la liste si le malloc réussi */
	list = (skip_list *) malloc(sizeof(skip_list));
	if(list){
		list->nbLevels = SKIPLIST_MAX_LEVEL;
		list->first = NULL;
	}
	else{
		list = NULL;
	}
	return list;
}

void skiplist_destruct(skip_list *list) {
    if(list == NULL)
        return;

    if(list->first == NULL){
        free(list);
        return;
    }

    for(int lvl=list->nbLevels-1;lvl>=0;lvl--) {
        skip_elt *elt = list->first[lvl];
        skip_elt **next = elt->next;
        free(elt);

        if(next != NULL) {
            elt = next[lvl];

            while (elt->next != NULL) {
                next = elt->next;
                free(elt);
                elt = next[lvl];
            }
            free(elt);
        }

    }
    free(list->first);
    free(list);

    list = NULL;

}

/*@ requires level>0;
 */
skip_elt **skipelt_create(int value, int level){

	skip_elt** e;

	/* 	Allocation du tableau */
	if( !(e = (skip_elt**) malloc(sizeof(skip_elt) * level)) ){
	//if( !(e = (skip_elt**) malloc(sizeof(skip_elt) * SKIPLIST_MAX_LEVEL)) ){
		perror("Erreur à l'allocation générale du premier élément\n");
		return NULL;
	}
	for(unsigned int i=0; i<level; i++){
		if( !(e[i] = (skip_elt*) malloc(sizeof(skip_elt))) ){
			fprintf(stderr, "Erreur à l'allocation du niveau %u du premier élément: %s\n", i, strerror(errno));
			/* Suppression de ce qui a pu être alloué */
			for(unsigned int j=0; j<i; j++){
				free(e[j]);
			}
			free(e);
			return NULL;
		}
		e[i]->value = value;
		e[i]->level = i;
		e[i]->next = NULL;
	}

	return e;

}

/*@	requires \valid(list);
	assigns \nothing;
	ensures (\result == 0 &&
		(list == \null ||
			count(elt, list->first) == 0) ) ||
		(\result == 1 &&
			count(elt, list->first) == 1);
 */
int skiplist_exist(skip_list *list, int elt){
	if(skiplist_search(list, elt))
		return 1;
	else
		return 0;
}

/*@ requires \valid(list);
	assigns \nothing;
	behavior is_in:
		assumes count(elt, list->first) == 1;
		ensures \result[0]->value == elt;
	behavior not_in:
		assumes count(elt, list->first) == 0;
		ensures \result == \null;
	complete behaviors is_in, not_in;
	disjoint behaviors is_in, not_in;
 */
// Retourne NULL si elt n'est pas présent
skip_elt **skiplist_search(skip_list *list, int elt){
	/* Si la liste n'est pas initialisée */
	if(list == NULL){
		return 0;
	}

	/* Si la liste est vide */
	if(list->first == NULL){
		return 0;
	}
	/* Sinon */
	int level = list->nbLevels-1;
	/* On définit l'itérateur <e> */
	skip_elt **e = list->first;

	while((e!=NULL) && (e[level]->value <= elt || level > 0)){
		while((level>0) && (e[level]->next == NULL)){
			level -= 1;
		}
		if(e[level]->value == elt){
			return e;
		}


		if(e[level]->next && e[level]->next[0]->value > elt && level>0){
			level -= 1;
		} else {
			e = e[level]->next;
		}
	}

	return NULL;
}

// is_in: si la valeur de l'élément suivant au même level est elt 
/*@ requires level >= 0;
	requires \valid(list);
	behavior is_in:
		assumes count(elt, list->first) == 1;
		ensures \result->next[level]->value == elt;
	behavior not_in:
		assumes count(elt, list->first) == 0;
		ensures \result == \null;
	complete behaviors is_in, not_in;
	disjoint behaviors is_in, not_in;
 */
skip_elt *skiplist_getLast(skip_list *list, int elt, int level) {
	/* Si la liste n'est pas initialisée */
	if(list == NULL){
		return NULL;
	}

	/* Si la liste est vide */
	else if (list->first == NULL) {
		return NULL;
	}

	/* Si le niveau demandé n'existe pas */
	else if (level >= list->nbLevels){
		return NULL;
	}

	/* Sinon */
	/* On définit l'itérateur <e> */
	else{
		skip_elt **e = list->first;
		skip_elt **last = list->first;

		while((e!=NULL) && (e[level]->value <= elt)){
			if(e[level]->value == elt){
				return last[level];
			}
			last = e;
			e = e[level]->next;
		}

		return last[level];
	}
}

/*@ requires \valid(list);
	requires p >= 0 && p <= 1;
	behavior is_in:
		assumes count(elt, list->first) == 1;
		ensures \result == 1 &&
			count(elt, list->first) == 1;
	behavior not_in:
		assumes count(elt, list->first) == 0;
		ensures (\result == 1 &&
					count(elt, list->first) == 1) ||
				(\result == 0 &&
					count(elt, list->first) == 0);
	complete behaviors is_in, not_in;
	disjoint behaviors is_in, not_in;
 */
int skiplist_insert(skip_list *list, int elt, double p){
    int ret = 0;
    int i;
    skip_elt **e;

    /* CAS 1  = Premier element de la liste */
    /* Initialisation de la liste (si necessaire) */
    if(list == NULL){
        list = skiplist_init();
        if(!list){
        	// Code mort
        	perror("Erreur à l'allocation de la liste");
        	return 0;
        }
    }
    /* On créé le premier élément */
    else if(list->first == NULL){
        list->nbLevels = SKIPLIST_MAX_LEVEL;
        list->first = skipelt_create(elt, list->nbLevels);
        if(list->first){
        	return 1;
        }
        else{
        	perror("Erreur à l'allocaion du premier élément");
        	return 0;
        }
    }

    /* CAS 2  = Element deja present */
    else if(skiplist_exist(list, elt)) {
        return 1;
    }

    /* CAS 3  = Le premier élément de la liste est supérieur à <elt> */
    else if(list->first[0]->value > elt){
        /* 	Élément à ajouter à la liste */
        int new_elt = list->first[0]->value;

        /* 	On remplace la valeur du premier élément */
        for(i=0; i<list->nbLevels; i++){
            list->first[i]->value= elt;
        }

        /* 	On appelle la fonction pour ajouter le premier élément qu'on a remplacé */
        ret = skiplist_insert(list, new_elt, p);
    }

    /* CAS GENERAL = On recherche la position et on ajoute */
    else{
	    e = list->first;
	    int level = list->nbLevels-1;

	    /* 	Garder une trace des pointeurs NULL */
	    skip_elt** tmp;
	    if( !(tmp = (skip_elt**) malloc(sizeof(skip_elt) * list->nbLevels)) ){
	        perror("Erreur à l'allocation générale du vecteur temporaire\n");
	        return EXIT_FAILURE;
	    }

	    /* 	On cherche la position de l'élément inférieur à <elt> le plus grand */
	    while( (e[0]->next!=NULL) && (e[0]->next[0]->value <= elt) ){
	        while((level>=0) && ((e[level]->next == NULL) || (e[level]->next[level]->value > elt))){
	            if( !(tmp[level] = (skip_elt*) malloc(sizeof(skip_elt))) ){
	                fprintf(stderr, "Erreur à l'allocation du niveau %d du vecteur temporaire: %s\n", level, strerror(errno));
	                return EXIT_FAILURE;
	            }
	            tmp[level]->next = e;
	            level -= 1;
	        }
	        if(level >= 0){
	            e = e[level]->next;
	        }
	    }

	    /* Creation de l'element  */
	    int nbLv = choose_level(p);
	    skip_elt** new_e = skipelt_create(elt, nbLv);

	    if(!new_e){
	    	return 0;
	    }

	    /* 	Définition des suivants du nouvel élément
	        et "branchement" des élément précédents
	    */
	    for(i=0; i<nbLv; i++){
	        if(i>level){
	            new_e[i]->next = tmp[i]->next[i]->next;
	            tmp[i]->next[i]->next = new_e;
	        }
	        else{
	            new_e[i]->next = e[i]->next;
	            e[i]->next = new_e;
	        }
	    }


	    // todo detruire tmp
	    //for(int i=0; i<list->nbLevels-1;i++){
	    //    free(tmp[i]);
	    //}
	    free(tmp);


	    ret = 1;

	    /* Si l'élément est présent, on ne change rien */
	}

	return ret;
}

/*@ requires \valid(list);
	behavior is_in:
		assumes count(elt, list->first) == 1;
		ensures \result == 1 &&
			count(elt, list->first) == 0;
	behavior not_in:
		assumes count(elt, list->first) == 0;
		ensures \result == 0 &&
			count(elt, list->first) == 0;
	complete behaviors is_in, not_in;
	disjoint behaviors is_in, not_in;
 */
int skiplist_delete(skip_list *list, int elt){

	/* CAS 1 : liste est vide */
	if(list->first == NULL){
		return 0;
	}
	else{
		/* CAS 2 : Si l'element n'existe pas */
		skip_elt **e = skiplist_search(list, elt);

		if(e == NULL) {
			return 0;
		}

		/* CAS 3 : Il faut supprimer le premier element */
		else if(list->first == e){
			return skiplist_delete_first(list);
		}
		else{
			/* CAS GENERAL */
			for(int lvl = 0; lvl<list->nbLevels; lvl++){
				skip_elt *last_one = skiplist_getLast(list,elt,lvl);

				if(last_one == NULL){
					printf("last_one null\n");
				}

				if(last_one != NULL && last_one->next != NULL && last_one->next[0]->value == e[0]->value)
					last_one->next = e[lvl]->next;
				/*else
					break;*/
			}
		
			return 1;
		}
	}
}

/*@ requires \valid(list);
 */
int skiplist_delete_first(skip_list *list){
	/* La liste n'est pas initialisée */
	if(list == NULL){
		return 0;
	}

	/* La liste est vide */
	if(list->first == NULL){
		return 0;
	}

	/* S'il n'y a qu'un élément dans la liste */
	if(list->first[0]->next == NULL){
		free(list->first);
	}
	else{
		skip_elt **next = list->first[0]->next;
       	skip_elt **e = list->first;
       	/*@ loop assigns lvl;
       	*/
		for(int lvl = 0; lvl<list->nbLevels; lvl++){
			// copie de la valeur de l'élément suivant
			e[lvl]->value = next[0]->value;
			// mise à jour du pointeur vers l'élément suivant
			if(e[lvl]->next != NULL && e[lvl]->next[lvl]->value == next[0]->value){
			   e[lvl]->next = e[lvl]->next[lvl]->next;
			}
		}
		free(next);
	}
	return 1;
}


/*@	ensures \result >= 1;
 */
int choose_level(double p){
	unsigned int i;
	int MAX_HEIGHT = SKIPLIST_MAX_LEVEL;
	/* Initialise le générateur d'aléatoire */
	//srand(time(NULL));

	/* Donne le niveau atteignable grâce à la probabilité */
	for(i = 1; (i < MAX_HEIGHT) && ((double) rand()/RAND_MAX) <= p; i++);

	return i;
}


void skiplist_print(skip_list *list){
	if(list == NULL){
		perror("Impossible d'afficher une liste non-initialisée\n");
		return;
	}

	if(list->first == NULL)
		return;

	/*@ requires list->nbLevels >= 0; */
	assert(list->nbLevels >= 0);
	int level  = list->nbLevels-1;

	/* Pour chaque niveau */
	while(level >= 0){
		skip_elt **e = list->first;
		skip_elt **e0 = list->first;

		// Affiche le premier element du level
		printf("%d", e[level]->value);
		printf("---");

		// On prend les elements suivants pour preparer la boucle
		e = e[level]->next;
		e0 = e0[0]->next;

		// tant que le niveau 0 n'est pas entièrement parcouru
		while(e0 != NULL) {
			// Si il reste des elements dans la ligne
			if (e != NULL) {
				// Il existe un element suivant aussi haut
				if ((*e)->value == (*e0)->value) {
					printf("%d", e[level]->value);
					printf("---");
					e = e[level]->next;
					e0 = e0[0]->next;
				}
					// Sinon
				else {
					printf("----");
					if((*e0)->value>9)
						printf("-");
					e0 = e0[0]->next;
				}
			}
				// Alors il n'y a plus d'element sur la ligne en cours
			else {
				printf("----");
				if((*e0)->value>9)
					printf("-");
				e0 = e0[0]->next;
			}
		}

		// une fois la ligne parcouru,
		// on descend d'un niveau et on affiche le NUL
		level--;
		printf("NUL\n");
	}
}
